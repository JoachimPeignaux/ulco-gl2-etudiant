"use strict";

importScripts ()

function repeatN (n, f) {
    for (let index = 1; index <= n; index++) {
        const node = document.createElement("li");
        node.innerHTML = `fibo(${index}) = ${f(index)}`;
        my_ul.appendChild(node);
    }
}