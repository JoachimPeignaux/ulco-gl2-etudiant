#include <cmath>

float sinus(float x, float a, float b) {
    return(sin(2*M_PI*(a*x+b)));
}

#include <emscripten/bind.h>

EMSCRIPTEN_BINDINGS(sinus) {
    emscripten::function("sinus", &sinus);
}
