#include <emscripten/bind.h>
#include <cmath>

struct Color {
    uint8_t _r;
    uint8_t _g;
    uint8_t _b;
    uint8_t _a;
};

class Sinus {

    private:
        int _width;
        int _height;
        std::vector<Color> _data;

    public:
        Sinus(int width, int height) : 
            _width(width), _height(height), _data(width*height) {
        }

        emscripten::val update(uint8_t frequence, uint8_t phase) { 

            for (int y=0; y<_height; y++)
            {
                for (int x=0; x<_width; x++)
                {
                    int coeff_vert = 255*( sin(x/frequence+phase) +1 )/2; //entre 0 et 1
                    Color c {0,uint8_t(coeff_vert), 0, 255};
                    _data[y*_width+x]=c;
                }
            }
            size_t s = _data.size()*4;
            uint8_t * d = (uint8_t *) _data.data();
            return emscripten::val(emscripten::typed_memory_view(s, d));
        }
};

EMSCRIPTEN_BINDINGS(sinus) {
    emscripten::class_<Sinus>("Sinus")
        .constructor<int, int>()
        .function("update", &Sinus::update);
}

