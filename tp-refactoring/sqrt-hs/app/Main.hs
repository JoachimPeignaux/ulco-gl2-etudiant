import Lib

main :: IO ()
main = do
    putStrLn "this is sqrt-hs"
    putStrLn $ mynegate $ mysqrt 1764
    putStrLn $ mynegate $ mysqrt (-1764)

mynegate :: Maybe Double -> String
mynegate x = case x of
    Just x -> show $ negate x
    Nothing -> "Error : negate value "