#include <fstream>
#include <functional>
#include <iostream>

const std::string path = "mylog1.txt";

int add3(int n) {
    return n+3;
}

int mul2(int n) {
    return n*2;
}

int mycompute(int v0) {
    std::cout << "add3 " + std::to_string(v0) << std::endl;
    const int v1 = add3(v0);
    std::cout << "mul2 " + std::to_string(v1) << std::endl;
    const int v2 = add3(v1);
    return v2;
}

int mycomputefile(std::ostream& flux, int v0 ) {
    flux << "add3 " + std::to_string(v0) << std::endl;
    const int v1 = add3(v0);
    flux << "mul2 " + std::to_string(v1) << std::endl;
    const int v2 = add3(v1);
    return v2;
}

int main() {
    std::ofstream flux;
    flux.open(path);
    std::cout << "this is log-cpp" << std::endl;

    const int res = mycomputefile(flux, 18);
    std::cout << res << std::endl;
    flux.close();
    return 0;
}


