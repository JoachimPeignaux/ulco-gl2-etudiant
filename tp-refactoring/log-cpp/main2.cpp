#include <fstream>
#include <functional>
#include <iostream>

using logFunc_t = std::function<void(const std::string &)>;
const std::string path = "mylog2.txt";

int add3(int n) {
    return n+3;
}

int mul2(int n) {
    return n*2;
}

int mycompute(logFunc_t flog, int v0) {

    flog("add3 " + std::to_string(v0));
    const int v1 = add3(v0);

    flog("mul2 " + std::to_string(v1));
    const int v2 = add3(v1);

    return v2;
}

int main() {
    std::cout << "this is log-cpp" << std::endl;

    logFunc_t logCout = [](const std::string & str) {
        std::cout << str <<std::endl;
    };

    std::ofstream flux;
    flux.open(path);

    logFunc_t logFichier = [&flux](const std::string & str) {
        flux << str <<std::endl;
    };

    mycompute(logCout,18);
    mycompute(logFichier,18);

    flux.close();
    return 0;
}