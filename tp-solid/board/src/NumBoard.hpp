#pragma once

#include "Board.hpp"

#include <iostream>

class NumBoard : public Board{
private : 
    unsigned int _name;

public:
    NumBoard() : _name(1) {};

    void add(const std::string & t)
    {
        std::string name = std::to_string(_name) + ". " + t;
        _name++;
        Board::add(name);
    }

    virtual std::string getTitle() const override {
        return "NumBoard";
    }
};
