#pragma once

#include <string>
#include <vector>

class Titleable {
    public:
        virtual std::string getTitle() const = 0;
};

