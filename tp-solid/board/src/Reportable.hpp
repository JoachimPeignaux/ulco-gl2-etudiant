#pragma once

#include <string>
#include <vector>

class Reportable {
public :
    virtual void report(Itemable&  itemable) = 0;
};