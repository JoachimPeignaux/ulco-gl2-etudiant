#pragma once
#include <fstream>
#include "Reportable.hpp"

class ReportFile : public Reportable{
private :
    std::ofstream _ofs;

public :
    ReportFile(const std::string & filename) : _ofs(filename) {}

    void report(Itemable&  itemable) {
        for (const std::string & item  : itemable.getItems())
            _ofs << item << std::endl;
        _ofs << std::endl;
    }
};