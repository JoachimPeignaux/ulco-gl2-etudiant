#include "Board.hpp"
#include "NumBoard.hpp"

void testBoard(Board & b, Reportable & rp) {
    std::cout << b.getTitle() << std::endl;
    b.add("item 1");
    b.add("item 2");
    rp.report(b);
}

int main() {

    NumBoard b1;
    ReportFile rp1("tmp.txt");
    ReportStdout rs1;
    testBoard(b1, rs1);

    return 0;
}

