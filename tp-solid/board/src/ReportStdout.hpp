#pragma once
#include <iostream>
#include "Reportable.hpp"

class ReportStdout : public Reportable{
    
public :
    void report(Itemable&  itemable) {
        for (const std::string & item : itemable.getItems())
            std::cout << item << std::endl;
        std::cout << std::endl;
    }
};