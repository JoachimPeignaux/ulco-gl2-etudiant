#pragma once

#include "Itemable.hpp"
#include "Titleable.hpp"
#include "ReportFile.hpp"
#include "ReportStdout.hpp"

class Board : public Itemable, public Titleable {
    protected:
        std::vector<std::string> _tasks;

    public:
        Board(){}

        void add(const std::string & t) {
            _tasks.push_back(t);
        }

        std::vector<std::string> getItems() const override {
            return _tasks;
        }

        virtual std::string getTitle() const override {
            return "Board";
        }
};
