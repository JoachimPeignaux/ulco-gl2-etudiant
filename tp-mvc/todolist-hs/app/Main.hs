import Board
import View

-- import Text.Read (readMaybe)

loop :: Board -> IO ()
loop b = do
    putStrLn ""
    printBoard b
    putStrLn ""
    input <- getLine
    let (action:phrase) = words input
    case action of
        "add" -> do
            let(_, b') = addTodo (unwords phrase) b
            putStrLn ("add ..." ++ (unwords phrase))
            loop b'
        "done" -> do
            putStrLn ("done ..."++ (unwords phrase))
            loop b
        "quit" -> putStrLn ""
        _ -> error "unknown choice"    -- TODO

main :: IO ()
main = loop newBoard

