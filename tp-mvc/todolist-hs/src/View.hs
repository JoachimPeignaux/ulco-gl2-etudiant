module View where
    
import Board
import Task

showTask :: Task -> String
showTask t = show (_taskId t) ++ ". " ++ (_taskName t)

printBoard :: Board -> IO ()
printBoard b = do
    putStrLn "TODO"
    let todoList = _boardTodo b
    mapM_ (\x -> putStrLn $ showTask x ) todoList
    putStrLn "DONE"
    let doneList = _boardDone b
    mapM_ (\x -> putStrLn $ showTask x ) doneList