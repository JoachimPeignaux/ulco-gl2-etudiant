#include <todolist-cpp/todolist-model-task.hpp>

Task::Task(std::string name) : _name(name)
{};

std::string Task::get_task()
{
    return _name;
}