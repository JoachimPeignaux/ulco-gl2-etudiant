#include <todolist-cpp/todolist-model-board.hpp>

std::vector<Task> _todo;
std::vector<Task> _done;

void Board::addTodo(std::string name)
{
    Task newTask(name);
    _todo.push_back(newTask);
}

void Board::addDone(std::string name)
{
    int pos = -1;
    for(int i=0; i<_done.size(); i++)
    {
        if(_todo[i].get_task() == name)
        {
            pos = i;
            break;
        }
    }
    if( pos != -1)
    {
        _todo.erase(_todo.begin() + pos);
        Task newTask(name);
        _done.push_back(newTask);
    }
    else
    {
        std::cout<<"task not in the todo list !!"<<std::endl;
    }
}
