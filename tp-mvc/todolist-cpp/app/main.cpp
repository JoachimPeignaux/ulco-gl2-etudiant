#include <todolist-cpp/todolist-view.hpp>
#include <todolist-cpp/todolist-model-board.hpp>
#include <todolist-cpp/todolist-model-task.hpp>

#include <iostream>

void mainLoop(Board& b)
{
    print(b);
    std::string choice, name;
    std::cout<<"Saisir 'Add' pour ajouter une tache, 'Done'pour en valider une, ou 'Quit' pour quitter l'application"<<std::endl;
    std::cin >> choice;
    if(choice == "Add")
    {
        std::cout<<"Veuillez saisir le nom de la tache :"<<std::endl;
        std::cin>>name;
        b.addTodo(name);
    }
    else if(choice == "Done")
    {
        std::cout<<"Veuillez saisir le nom de la tache :"<<std::endl;
        std::cin>>name;
        b.addDone(name);
    }
    else if(choice == "Quit")
    {
        return;
    }
    else
    {
        std::cout<<"Veuillez saisir un choix valide"<<std::endl;
    }
    mainLoop(b);
}

int main() {
    Board board;
    mainLoop(board);
}

