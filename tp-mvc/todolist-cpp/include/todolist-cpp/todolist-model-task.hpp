#pragma once
#include <todolist-cpp/todolist-model-task.hpp>
#include <string>

class Task{
private:
    std::string _name;
public:
    Task(std::string name);
    std::string get_task();
};