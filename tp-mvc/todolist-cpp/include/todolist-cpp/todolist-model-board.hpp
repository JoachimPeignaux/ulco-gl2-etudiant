#pragma once
#include <vector>
#include <todolist-cpp/todolist-model-task.hpp>
#include <iostream>

class Board{
    friend void print( Board & );
private:
    std::vector<Task> _todo;
    std::vector<Task> _done;

public:
    void addTodo(std::string name);
    void addDone(std::string name);
};