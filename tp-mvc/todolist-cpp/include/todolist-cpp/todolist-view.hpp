#pragma once
#include <todolist-cpp/todolist-model-board.hpp>
#include <todolist-cpp/todolist-model-task.hpp>
#include <iostream>
void print(Board & b)
{
    std::cout<<"=== TODO ==="<<std::endl;
    for(unsigned int i=0; i<b._todo.size(); i++)
    {
        std::cout<<i<<" ."<<b._todo[i].get_task()<<std::endl;
    }
    std::cout<<std::endl;
    std::cout<<"=== DONE ==="<<std::endl;
    for(unsigned int i=0; i<b._done.size(); i++)
    {
        std::cout<<i<<" ."<<b._done[i].get_task()<<std::endl;
    }
    std::cout<<std::endl;
}